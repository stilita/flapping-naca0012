 #!/bin/bash

. /usr/lib/openfoam/openfoam2212/etc/bashrc

cd Fluid

decomposePar
mpirun -np 8 renumberMesh -overwrite -parallel | tee log.renumberMesh 2>&1
mpirun -np 8 overSimpleFoam -parallel | tee log.solver 2>&1
reconstructPar -latestTime
