#!/usr/bin/env python

###
### This file is generated automatically by SALOME v9.10.0 with dump python functionality
###

import sys
import salome

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()
sys.path.insert(0, r'/home/claudio/Projects/Studies/over-flapping-naca/20_geom')

####################################################
##       Begin of NoteBook variables section      ##
####################################################
notebook.set("x1", -0.1)
notebook.set("x2", 0.5)
notebook.set("y1", -0.15)
notebook.set("y2", 0.15)
notebook.set("z1", 0)
notebook.set("z2", 0.5)
####################################################
##        End of NoteBook variables section       ##
####################################################
###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS

import numpy as np


geompy = geomBuilder.New()

O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)

c = 0.1
t = 0.12
delta_x = c/2
z0 = 0.0

numpoints = 100

ang_rem = 0

beta = np.linspace(0, np.pi-ang_rem*np.pi/180.0, numpoints)

chord_points = (1 - np.cos(beta)) / 2.0

points = []

# counter clockwise order!
for x in chord_points:
    y = 5 * t * c * (
            0.2969 * np.sqrt(x) - 0.126 * x - 0.3516 * pow(x, 2) + 0.2843 * pow(x, 3) - 0.1015 * pow(x, 4))
    point = np.array([x * c - delta_x, y, z0])
    
    points.append(point)

Vertices_up = []
Vertices_down = []

for p in points:
    Vertex1 = geompy.MakeVertex(p[0], p[1], p[2])
    Vertex2 = geompy.MakeVertex(p[0], -p[1], p[2])
    Vertices_up.append(Vertex1)
    Vertices_down.append(Vertex2)



#Curve_1 = geompy.MakePolyline([Vertex_1, Vertex_2, Vertex_3], False)
Curve_up = geompy.MakeInterpol(Vertices_up, False, False)
Curve_down = geompy.MakeInterpol(Vertices_down, False, False)

trailing_line = geompy.MakeLineTwoPnt(Vertices_up[-1], Vertices_down[-1])
naca0012_profile = geompy.MakeWire([Curve_up, Curve_down, trailing_line], 1e-07)
naca0012_surface = geompy.MakeFaceWires([naca0012_profile], 1)

wing = geompy.MakePrismVecH(naca0012_surface, OZ, 0.3)

geompy.ExportSTL(wing, "/home/claudio/Projects/Studies/over-flapping-naca/20_geom/naca0012.stl", True, 1e-06, True)

faces = geompy.SubShapeAllSortedCentresIDs(wing, geompy.ShapeType["FACE"])


geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Vertices_up[-1], 'trailing_up' )
geompy.addToStudy( Vertices_down[-1], 'trailing_down' )
#geompy.addToStudy( Vertex_2, 'Vertex_2' )
#geompy.addToStudy( Vertex_3, 'Vertex_3' )
#geompy.addToStudy( Curve_1, 'Curve_1' )
geompy.addToStudy( Curve_up, 'Curve_up' )
geompy.addToStudy( Curve_down, 'Curve_down' )
geompy.addToStudy( trailing_line, 'trailing_line' )
geompy.addToStudy( naca0012_profile, 'naca 0012 profile' )
geompy.addToStudy( naca0012_surface, 'naca 0012 surface' )
geompy.addToStudy( wing, 'naca 0012 wing' )

for i, f in enumerate(faces):
    geompy.addToStudyInFather(wing, f, 'face' )


if salome.sg.hasDesktop():
  salome.sg.updateObjBrowser()
