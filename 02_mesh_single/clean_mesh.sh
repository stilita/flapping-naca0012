#!/bin/sh

#cp -r 0.orig/ 0/

rm -rfv processor*
cd constant

cd triSurface
rm -rfv *.eMesh
cd ..

rm -rfv polyMesh
rm -rfv extendedFeatureEdgeMesh

cd ..
