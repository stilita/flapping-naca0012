#!/bin/sh

#cp -r 0.orig/ 0/

echo "Single mesh"

blockMesh
surfaceFeatureExtract
decomposePar
mpirun -np 8 snappyHexMesh -parallel
reconstructParMesh

#echo "what is extrudeMesh? Is it needed?"
#? extrudeMesh

#echo "createPatch dictionary?"
#? createPatch -overwrite


echo "Check Mesh"

checkMesh -latestTime |  tee log.checkMesh


