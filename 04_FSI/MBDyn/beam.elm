# $Header: /var/cvs/mbdyn/mbdyn/mbdyn-web/documentation/examples/beam.elm,v 1.2 2008/11/05 20:47:14 masarati Exp $

# body: BODY_LABEL, NODE_LABEL,
#    mass
#    reference node offset
#    inertia tensor



body: 1000+curr_node, curr_node,
	condense, 2,
	1.50000000e-06*RHO,
	reference, node, null,
	diag, +2.81250000e-11*RHO, +2.81250000e-11*RHO, +1.25000000e-09*RHO,
	1.04606400e-05*RHO_PDMS,
	reference, node, -7.76832727e-03, +0.00000000e+00, +0.00000000e+00,
	diag, +1.96137000e-10*RHO_PDMS, +1.96137000e-10*RHO_PDMS, +8.71720000e-09*RHO_PDMS;

body: 1000+curr_node+1, curr_node+1,
	condense, 2,
	1.50000000e-06*RHO,
	reference, node, null,
	diag, +2.81250000e-11*RHO, +2.81250000e-11*RHO, +1.25000000e-09*RHO,
	1.04606400e-05*RHO_PDMS,
	reference, node, -7.76832727e-03, +0.00000000e+00, +0.00000000e+00,
	diag, +1.96137000e-10*RHO_PDMS, +1.96137000e-10*RHO_PDMS, +8.71720000e-09*RHO_PDMS;




# beam 3 nodes
# NODE1_LABEL, offset
# NODE2_LABEL, offset
# NODE3_LABEL, offset
# orientation from node
# CONSTUTIVE LAW: EA, GAy, GAz, GJ, EJy, EJz
#linear elastic generic, diag,
#EA, GAy, GAz, GJ, EJy, EJz,


beam3: 100+curr_node,
	curr_node - 1, null,
	curr_node, null,
	curr_node + 1, null,
	1, 0., 0., 1., 3, 0., 1., 0., 
	reference, BEAM_CL,
	same,
	same;
