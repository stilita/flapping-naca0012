# flapping-naca0012


## 01 Gemetry

This folder contains some scripts and Salome studies used to generate the NACA 0012 airfoil according the experimental dimensions

## 02 Mesh for single domain

This Folder contains the OpenFOAM files used to generate the fluid mesh for the sigle domain. Scripts *run_mesh.sh* and *clean_mesh.sh* are used to start the computation and clean data. The mesh generated here is used in the following cases.

## 03 Initialization fr single domain

This folder contanins a steady state case used to initialize the fluid flow. The script *run_solver.sh* performs the computation. The mesh must be generated in the previous step.

## 04 FSI simulation

This folder contains the simulation of the FSI case. Subfolder _Fluid_ must contain the mesh and the initial conditions generated in the previous steps. The subfolder _MBDyn_ contains the multibody simulation. The coupled simulation must be started from the main directory, either using the script *run_solver.sh* or by starting the two simulations in separate shells. In the latter case the script *run_fluid.sh* statrs the Fluid simulation (the case must be decomposed before), while the command *mbbdyn-esm-adapter -f config.json* launches the multibody simulation.

## 11 Mesh for overset case

This folder contains the OpenFOAM files used to generate and overset mesh for the NACA0012 wing. The script *run_mesh.sh* performs all the required operations.

## 12 Rigid overset

This case is used, as for the single domain, to generate initialization data for the FSI simulation. The overset mesh is fixed. The mesh is generated at previous step. The first script to be run  is *run_mesh.sh* (it chooses the 0 directory among 3 possible inlet velocities). Then *run_solver.sh* must be run. 

## 13 Rigid flapping

This case performs an overset simulation with a rigid flapping wing. The mesh and the 0 directory must be retrieved from the previous cases.

# 14 FSI overset

This case performs an FSI simulation of a flexible wing with overset. Mesh and initial fluid flow must be retrieved from previous cases.  The first script to be run  is *run_mesh.sh*. Then *run_solver.sh* must be run.
