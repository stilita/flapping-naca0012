 #!/bin/bash

. /usr/lib/openfoam/openfoam2212/etc/bashrc

cd Fluid

decomposePar
#mpirun -np 32 --oversubscribe renumberMesh -overwrite -parallel -allRegions 2>&1 | tee log.renumberMesh 
mpirun -np 32 --oversubscribe overPimpleDyMFoam -parallel 2>&1 | tee log.solver 
#reconstructPar -latestTime
