
body: 1000+curr_node, curr_node,
	condense, 2,
	1.50000000e-06*RHO,
	reference, node, null,
	diag, +2.81250000e-11*RHO, +2.81250000e-11*RHO, +1.25000000e-09*RHO,
	1.04606400e-05*RHO_PDMS,
	reference, node, -7.76832727e-03, +0.00000000e+00, +0.00000000e+00,
	diag, +1.96137000e-10*RHO_PDMS, +1.96137000e-10*RHO_PDMS, +8.71720000e-09*RHO_PDMS;

body: 1000+curr_node+1, curr_node+1,
	condense, 2,
	7.50000000e-07*RHO,
	reference, node, +0.00000000e+00, +0.00000000e+00, -3.75000000e-03,
	diag, +3.51562500e-12*RHO, +3.51562500e-12*RHO, +6.25000000e-10*RHO,
	5.23032000e-06*RHO_PDMS,
	reference, node, -7.76832727e-03, +0.00000000e+00, -3.75000000e-03,
	diag, +2.45171250e-11*RHO_PDMS, +2.45171250e-11*RHO_PDMS, +4.35860000e-09*RHO_PDMS;

beam3: 100+curr_node,
	curr_node - 1, null,
	curr_node, null,
	curr_node + 1, null,
	1, 0., 0., 1., 3, 0., 1., 0., 
	reference, BEAM_CL,
	same,
	same;
