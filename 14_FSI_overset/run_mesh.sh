#!/bin/sh

#cp -r 0.orig/ 0/

. /usr/lib/openfoam/openfoam2212/etc/bashrc

cd ./Fluid

topoSet | tee log.topoSet
#topoSet -dict system/topoSetDict_movingZone | tee log.topoSetMZ

echo "Restore 0 dir"

rm -r 0

cp -r ./0.ss_03ms 0

echo "change Dictionary for U"
changeDictionary 2>&1 | tee log.changeDict


echo "SetFields"

setFields | tee log.setFields

echo "Check Mesh"

checkMesh |  tee log.checkMesh

cd ..
