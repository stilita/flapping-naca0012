#!/bin/bash

. /usr/lib/openfoam/openfoam2212/etc/bashrc

decomposePar -case Fluid
#mpirun -np 32 --oversubscribe renumberMesh -overwrite -parallel -allRegions 2>&1 | tee log.renumberMesh 


#--oversubscribe 
mpirun -np 32 overPimpleDyMFoam -parallel -case Fluid 2>&1 | tee log.solver 


#reconstructPar -latestTime
