#!/bin/sh

#cp -r 0.orig/ 0/

cd wing

rm -rfv processor*
cd constant

cd triSurface
rm -rfv *.eMesh
cd ..

rm -rfv polyMesh
rm -rfv extendedFeatureEdgeMesh

cd ../..

cd Fluid
cd constant

rm -rfv polyMesh

cd ../..
