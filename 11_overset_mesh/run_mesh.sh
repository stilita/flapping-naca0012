#!/bin/sh

#cp -r 0.orig/ 0/

echo "check flapping_airfoil, case named foil_shmmesh"

cd wing

blockMesh
surfaceFeatureExtract
decomposePar
mpirun -np 8 snappyHexMesh -parallel -overwrite
reconstructParMesh -constant

#snappyHexMesh -overwrite

#echo "what is extrudeMesh? Is it needed?"
#? extrudeMesh

#echo "createPatch dictionary?"
#? createPatch -overwrite

cd ..


echo "Build background mesh"

cd Fluid
blockMesh

echo "merge"

mergeMeshes . ../wing -overwrite

echo "apply toposet"

topoSet | tee log.topoSet

# non serve?
#topoSet -dict system/topoSetDict_movingZone

echo "Restore 0 dir"

rm -r 0

cp -r 0.orig 0

echo "SetFields"

setFields | tee log.setFields

echo "Check Mesh"

checkMesh |  tee log.checkMesh


cd ..
